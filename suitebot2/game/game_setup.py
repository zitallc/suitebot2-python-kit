from typing import Iterable, Tuple

from suitebot2.game.point import Point


class GameSetup:
    def __init__(self,
                 ai_player_id: int,
                 player_ids: Iterable[int],
                 plan_width: int,
                 plan_height: int,
                 starting_positions: Iterable[Point],
                 max_rounds: int):
        self._ai_player_id = ai_player_id
        self._player_ids = tuple(player_ids)
        self._plan_width = plan_width
        self._plan_height = plan_height
        self._starting_positions_map = dict(zip(player_ids, starting_positions))
        self._max_rounds = max_rounds

    def get_ai_player_id(self) -> int:
        return self._ai_player_id

    def get_player_ids(self) -> Tuple[int]:
        return self._player_ids

    def get_plan_width(self) -> int:
        return self._plan_width

    def get_plan_height(self) -> int:
        return self._plan_height

    def get_starting_position(self, player_id: int) -> Point:
        if player_id not in self._player_ids:
            raise ValueError("unknown player ID: %i" % player_id)
        return self._starting_positions_map.get(player_id)

    def get_max_rounds(self) -> int:
        return self._max_rounds
