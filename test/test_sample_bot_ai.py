import unittest

from suitebot2.ai.sample_bot_ai import SampleBotAi
from suitebot2.game.game_round import GameRound
from suitebot2.game.game_setup import GameSetup
from suitebot2.game.player_move import PlayerMove
from suitebot2.game.point import Point


class TestSampleBotAi(unittest.TestCase):
    GAME_SETUP = GameSetup(
        ai_player_id=1,
        player_ids=[1, 2, 3],
        plan_width=10,
        plan_height=5,
        starting_positions=[Point(1, 1), Point(2, 2), Point(3, 3)],
        max_rounds=4)

    def test_should_fire_if_enemy_fired(self):
        bot_ai = SampleBotAi()
        bot_ai.initialize_and_make_move(self.GAME_SETUP)
        my_move_after_enemy_fired = bot_ai.make_move(self._make_game_round("MY MOVE", "FIRE", "WARM UP"))
        self.assertEqual(my_move_after_enemy_fired, "FIRE")

    def test_should_not_fire_if_enemy_did_not_fire(self):
        bot_ai = SampleBotAi()
        my_first_move = bot_ai.initialize_and_make_move(self.GAME_SETUP)
        self.assertNotEqual(my_first_move, "FIRE")
        my_move_after_enemy_did_not_fire = bot_ai.make_move(self._make_game_round("MY MOVE", "WARM UP", "WARM UP"))
        self.assertNotEqual(my_move_after_enemy_did_not_fire, "FIRE")

    def test_should_warm_up_in_first_half(self):
        bot_ai = SampleBotAi()
        game_round = self._make_game_round("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE")
        my_round1_move = bot_ai.initialize_and_make_move(self.GAME_SETUP)
        self.assertEqual(my_round1_move, "WARM UP")
        my_round2_move = bot_ai.make_move(game_round)
        self.assertEqual(my_round2_move, "WARM UP")

    def test_should_no_longer_warm_up_in_second_half(self):
        bot_ai = SampleBotAi()
        game_round = self._make_game_round("MY MOVE", "ENEMY1 MOVE", "ENEMY2 MOVE")
        bot_ai.initialize_and_make_move(self.GAME_SETUP)
        bot_ai.make_move(game_round)
        my_round3_move = bot_ai.make_move(game_round)
        self.assertNotEqual(my_round3_move, "WARM UP")
        my_round4_move = bot_ai.make_move(game_round)
        self.assertNotEqual(my_round4_move, "WARM UP")

    def _make_game_round(self, *moves: str) -> GameRound:
        return GameRound([PlayerMove(player_id, move) for (player_id, move)
                          in zip(self.GAME_SETUP.get_player_ids(), moves)])
