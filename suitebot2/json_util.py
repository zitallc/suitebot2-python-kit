import json

from suitebot2.game.game_round import GameRound
from suitebot2.game.game_setup import GameSetup
from suitebot2.game.player_move import PlayerMove
from suitebot2.game.point import Point


def decode_message_type(request_json: str) -> str:
    request = json.loads(request_json)
    return request['messageType'].upper()


def decode_moves_message(request_json: str) -> GameRound:
    request = json.loads(request_json)
    return GameRound([PlayerMove(move['playerId'], move['move']) for move in request['moves']])


def decode_setup_message(request_json: str) -> GameSetup:
    request = json.loads(request_json)
    return GameSetup(
        ai_player_id=request['aiPlayerId'],
        player_ids=[player['id'] for player in request['players']],
        plan_width=request['gamePlan']['width'],
        plan_height=request['gamePlan']['height'],
        starting_positions=[Point(point['x'], point['y']) for point in request['gamePlan']['startingPositions']],
        max_rounds=request['gamePlan']['maxRounds']
    )
