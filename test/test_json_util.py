import unittest

from suitebot2 import json_util
from suitebot2.game.point import Point


class TestJsonUtil(unittest.TestCase):
    SETUP_MESSAGE = """
    {
            "messageType":"setup",
            "aiPlayerId":1,
            "players":[{"id":2,"name":"R2D2"},{"id":1,"name":"C-3PO"}],
            "gamePlan":{
                "width":10,
                "height":20,
                "startingPositions":[{"x":2,"y":2},{"x":7,"y":7}],
                "maxRounds":25
            }
    }
    """

    MOVES_MESSAGE = """
    {
        "messageType":"moves",
        "moves":[{"playerId":1,"move":"FIRE"},{"playerId":2,"move":"JUMP"}]
    }
    """

    def test_decode_message_type(self):
        self.assertEqual(json_util.decode_message_type(self.SETUP_MESSAGE), "SETUP")
        self.assertEqual(json_util.decode_message_type(self.MOVES_MESSAGE), "MOVES")

    def test_decode_moves_message(self):
        game_round = json_util.decode_moves_message(self.MOVES_MESSAGE)
        self.assertEqual(len(game_round.get_moves()), 2)
        self.assertEqual(game_round.get_moves()[0].player_id, 1)
        self.assertEqual(game_round.get_moves()[0].move, "FIRE")
        self.assertEqual(game_round.get_moves()[1].player_id, 2)
        self.assertEqual(game_round.get_moves()[1].move, "JUMP")

    def test_decode_setup_message(self):
        game_setup = json_util.decode_setup_message(self.SETUP_MESSAGE)
        self.assertEqual(game_setup.get_ai_player_id(), 1)
        self.assertEqual(game_setup.get_player_ids(), (2, 1))
        self.assertEqual(game_setup.get_plan_width(), 10)
        self.assertEqual(game_setup.get_plan_height(), 20)
        self.assertEqual(game_setup.get_starting_position(1), Point(7, 7))
        self.assertEqual(game_setup.get_starting_position(2), Point(2, 2))
        self.assertEqual(game_setup.get_max_rounds(), 25)
