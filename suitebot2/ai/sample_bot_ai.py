from suitebot2.ai.bot_ai import BotAi
from suitebot2.game.game_round import GameRound
from suitebot2.game.game_setup import GameSetup
from suitebot2.game.player_move import PlayerMove
from suitebot2.game.point import Point


class SampleBotAi(BotAi):
    my_id = None  # type: int
    my_position = None  # type: Point
    max_rounds = None  # type: int
    round = None  # type: int
    enemy_fired = False

    def initialize_and_make_move(self, game_setup: GameSetup) -> str:
        self.my_id = game_setup.get_ai_player_id()
        self.my_position = game_setup.get_starting_position(self.my_id)
        self.max_rounds = game_setup.get_max_rounds()
        self.round = 1
        return self.figure_out_move()

    def make_move(self, game_round: GameRound) -> str:
        self.round += 1
        self.enemy_fired = False
        for player_move in game_round.get_moves():
            self.simulate_move(player_move)
        return self.figure_out_move()

    def simulate_move(self, player_move: PlayerMove):
        if player_move.player_id != self.my_id and player_move.move == "FIRE":
            self.enemy_fired = True

    def figure_out_move(self) -> str:
        if self.enemy_fired:
            # If they have fired, we will fire too!
            return "FIRE"
        else:
            if self.round <= self.max_rounds / 2:
                return "WARM UP"
            else:
                return "GO LEFT"
