from abc import ABCMeta, abstractmethod

from suitebot2.game.game_round import GameRound
from suitebot2.game.game_setup import GameSetup


class BotAi:
    __metaclass__ = ABCMeta

    @abstractmethod
    def initialize_and_make_move(self, game_setup: GameSetup) -> str:
        """This method is called at the beginning of the game.

        :param game_setup: details of the game, such as the game plan dimensions, the list of players, etc
        :return the first move to play
        """

    @abstractmethod
    def make_move(self, game_round: GameRound) -> str:
        """This method is called in each round, except for the first one.

        :param game_round all players' moves in the previous round
        :return the move to play
        """